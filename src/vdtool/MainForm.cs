﻿/*
 * Erstellt mit SharpDevelop.
 * Benutzer: e01a5g22
 * Datum: 02.02.2009
 * Zeit: 08:57
 * 
 * Sie können diese Vorlage unter Extras > Optionen > Codeerstellung > Standardheader ändern.
 */
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Test2
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		SQLiteConnection connection = new SQLiteConnection();
		
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
		}
		
		
		
		
		

		
		
		void OpenVDClick(object sender, EventArgs e)
		{
			string vdLine="";
			long lineno=0;
			string[] vdField;
			int fieldcount=0, n;
			string[] fieldType=new string[50];
			
			
			
			openFileDialog1.DefaultExt="vd_";		// Dateinamenerweiterung ist .vd_, wenn nichts anderes angegeben
			openFileDialog1.Filter="vd_ files (*.vd_)|*.vd_|All Files (*.*)|*.*";	// Filtereinträge für Dateitypen
			openFileDialog1.Title="Open vd_ file";	// Titel des "Datei öffnen" Fensters
			openFileDialog1.ShowDialog();			// Dialog-Fenster für "Datei öffnen" öffnen
			string vdname=openFileDialog1.FileName;	// vdname enthält Dateinamen mit Pfad
			if (vdname != "") {
				toolStripStatusLabel1.Text="vd_ file is beeing converted to database";
				string dbname=vdname.Substring(0,vdname.Length - 3)+"db_";
				label1.Text=dbname;
				connection.Close();						// erstmal Verbindung schliessen
				toolStripStatusLabel1.Text=dbname;		// Datenbanknamen anzeigen
				SQLiteConnection.CreateFile(dbname);	// leere Datenbank anlegen, wenn sie schon existiert wird sie überschrieben!
				connection.ConnectionString = "Data Source=" + dbname;  
   				connection.Open();
   				
				StreamReader objReader = new StreamReader(vdname);
				SQLiteCommand command = new SQLiteCommand(connection);
				SQLiteCommand command2 = new SQLiteCommand(connection);
				SQLiteCommand command3 = new SQLiteCommand(connection);
				
				char[] delimit = new char[] { ' ' };	// Trennzeichen
				command.CommandText="";
				
				while (vdLine != null) {
					vdLine = objReader.ReadLine();
					if (vdLine != null) {
						lineno++;
						vdField=vdLine.Split(delimit);
						if (vdLine.Length > 2) {
							if (vdField[0] == "T") {
								fieldcount=0;
								label1.Text="Table: "+vdField[1];
								Application.DoEvents();	//Events abarbeiten, sonst wird während der Schleife nix angezeigt
								// Erstellen der Tabelle, sofern diese noch nicht existiert.  
								command.CommandText = "CREATE TABLE IF NOT EXISTS T" + vdField[1] + " ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "; //name VARCHAR(100) NOT NULL);";
								
								do {
									vdLine = objReader.ReadLine();
									vdField=vdLine.Split(delimit);	//vdLine in Felder zerlegen (durch " " getrennt)
									if (vdLine.Substring(0,1)=="C") {	// C in der vd_ leitet die Definition der Felder einer Tabelle ein
										if (vdField[0].Substring(1)!="1") command.CommandText+=", ";
										command.CommandText += vdField[5];
										if (vdField[2]=="1" || vdField[2]=="2") {	//Feldtyp 1 oder 2 ist integer
											command.CommandText+=" INTEGER";
											fieldType[fieldcount]="i";
										}
										if (vdField[2]=="3" || vdField[2]=="8") {
											command.CommandText += " VARCHAR(" + vdField[3] + ")";
											fieldType[fieldcount]="s";
										}
										fieldcount++;
									}
								} while (vdLine.Substring(0,1)=="C");
								command.CommandText += ");";
								command.ExecuteNonQuery();
								//listBoxRecords.Items.Add(command.CommandText);
								command.CommandText="";
							}
							if (vdField[0] == "R") {
								label2.Text="Row : "+vdField[1];
								Application.DoEvents();	//Events abarbeiten, sonst wird während der Schleife nix angezeigt

								command.CommandText = "INSERT INTO " + vdField[2] + vdField[3] + " VALUES(null, ";
								for(n=0;n<fieldcount;n++) {
									vdLine = objReader.ReadLine();
									if (vdLine.Length>1 && vdLine.Substring(0,2)=="\\\\") {
										n--;
										command.CommandText=command.CommandText.Substring(0,command.CommandText.Length-1);
									}
									else {
										if (n>0) command.CommandText += ", ";
										if (fieldType[n]=="s") command.CommandText += "'";
									}
									if (vdLine!="") command.CommandText += vdLine.Replace("\\\'","");
									else if (fieldType[n]=="i") command.CommandText += "0";
									if (fieldType[n]=="s") command.CommandText += "'";
								}
								command.CommandText += ")";
								//listBoxRecords.Items.Add(command.CommandText);
								try {
									command.ExecuteNonQuery();
								}
								catch (Exception) {
									listBoxErrors.Items.Add("FEHLER:" + command.CommandText);
								}
								
							}
						}
					}
				}
				objReader.Close();
				
				AnalyzeDB();

			}
		}
		
		
		void OpenDBClick(object sender, EventArgs e)
		{
			openFileDialog1.DefaultExt="db_";		// Dateinamenerweiterung ist .vd_, wenn nichts anderes angegeben
			openFileDialog1.Filter="vd_ files (*.db_)|*.db_|All Files (*.*)|*.*";	// Filtereinträge für Dateitypen
			openFileDialog1.Title="Open db_ file";	// Titel des "Datei öffnen" Fensters
			openFileDialog1.ShowDialog();			// Dialog-Fenster für "Datei öffnen" öffnen
			string dbname=openFileDialog1.FileName;	// vdname enthält Dateinamen mit Pfad
			if (dbname != "") {
				connection.Close();						// erstmal Verbindung schliessen
				toolStripStatusLabel1.Text=dbname;		// Datenbanknamen anzeigen
				connection.ConnectionString = "Data Source=" + dbname;  
   				connection.Open();
   				AnalyzeDB();
			}

		}
		
		void AnalyzeDB()
		{
			int old_address=0, old_paramtype=0, old_offset=0;
			int parent_id=0;
			string old_name;
			
			SQLiteCommand command = new SQLiteCommand(connection);
			SQLiteCommand command2 = new SQLiteCommand(connection);
			SQLiteCommand command3 = new SQLiteCommand(connection);

				Result.Text= "File created by kubi's vdTool 2.0\n";
				Result.Text+="---------------------------------\n\n";
				command.CommandText="SELECT * FROM T7";
				SQLiteDataReader reader = command.ExecuteReader();
				reader.Read();
				Result.Text+="Product Name ............ : " + reader[4] + "\n";
				Result.Text+="Serial Number ........... : " + reader[9] + "\n";
				Result.Text+="Manufacturer ID ......... : 0x"+String.Format("{0:X2}", reader[2]) + "\n";
				reader.Dispose();  
				reader.Close();
				
			
				command.CommandText="SELECT * FROM T10";
				reader = command.ExecuteReader();
				reader.Read();
				Result.Text+="Mask Version ............ : 0x"+String.Format("{0:X4}", reader[2])+"\n";				
				Result.Text+="User Ram Start .......... : 0x"+String.Format("{0:X4}", reader[3])+"\n";
				Result.Text+="User Ram End ............ : 0x"+String.Format("{0:X4}", reader[4])+"\n";
				Result.Text+="EEPROM Start ............ : 0x"+String.Format("{0:X4}", reader[5])+"\n";
				Result.Text+="EEPROM End .............. : 0x"+String.Format("{0:X4}", reader[6])+"\n";
				Result.Text+="RUN Error Adress ........ : 0x"+String.Format("{0:X4}", reader[7])+"\n";
				Result.Text+="Address Table Address ... : 0x"+String.Format("{0:X4}", reader[8])+"\n";
				Result.Text+="Association Table Address : 0x"+String.Format("{0:X4}", reader[9])+"\n";
				Result.Text+="Commstab Pointer Address  : 0x"+String.Format("{0:X4}", reader[10])+"\n";
				
				// Beenden des Readers und Freigabe aller Ressourcen.  
				reader.Dispose();  
				reader.Close();
				
				command.CommandText="SELECT * FROM T12";
				reader = command.ExecuteReader();
				reader.Read();
				Result.Text+="Device Type ............. : 0x"+String.Format("{0:X4}", reader[8]) + "\n";
				Result.Text+="Commstab Address ........ : 0x"+String.Format("{0:X2}", Convert.ToInt32(reader[13])-256) + "\n\n";
				
				reader.Dispose();  
				reader.Close();
				

				
				command.CommandText="SELECT * FROM T12";
				reader = command.ExecuteReader();
				SQLiteDataReader reader2;
				SQLiteDataReader reader3;
				while (reader.Read()) {
					Result.Text+="Application Program " + reader[1].ToString() + "  " + reader[4].ToString() + "\n";
					Result.Text+="--------------------------------------------------\n";
					command2.CommandText="SELECT * FROM T23 WHERE PROGRAM_ID=" + reader[1].ToString() + " ORDER BY PARAMETER_ADDRESS,PARAMETER_BITOFFSET";
					reader2 = command2.ExecuteReader();
					while (reader2.Read()) {
						if (old_address!=Convert.ToInt32(reader[11]) || old_paramtype!=Convert.ToInt32(reader2[2]) || old_offset!=Convert.ToInt32(reader2[12])) {
							old_address=Convert.ToInt32(reader[11]);
							old_paramtype=Convert.ToInt32(reader2[2]);
							old_offset=Convert.ToInt32(reader2[12]);
							if (Convert.ToInt16(reader2[11])!=0) {	// wenn parameter_address nicht null
								Result.Text+="0x" + String.Format("{0:X4}", reader2[11]) + "  Bit " + (7-Convert.ToInt16(reader2[12])-Convert.ToInt16(reader2[8])+1).ToString() + "-" + (7-Convert.ToInt32(reader2[12])).ToString() + "  Param-ID: " + reader2[14] + "  " + reader2[4] + "  ";
								
								// Alle Parent Parameter Namen anzeigen
								parent_id=Convert.ToInt32(reader2[15]);
								old_name=reader2[4].ToString();
								while(parent_id!=0) {
									command3.CommandText="SELECT * FROM T23 WHERE PARAMETER_ID=" + parent_id.ToString();
									reader3 = command3.ExecuteReader();
									parent_id=Convert.ToInt32(reader3[15]);
									if(old_name!=reader3[13].ToString()) {	//keine doppelten anzeigen
										Result.Text+=reader3[13]+"  ";
										old_name=reader3[13].ToString();
									}
									reader3.Dispose();  
									reader3.Close();
								}
								Result.Text+="\n";
								
								// aus T22 die Parameterwerte holen und anzeigen
								command3.CommandText="SELECT * FROM T22 WHERE PARAMETER_TYPE_ID=" + reader2[2].ToString() + " ORDER BY REAL_VALUE";
								reader3 = command3.ExecuteReader();
								while (reader3.Read()) {
									Result.Text+="  " + reader3[2] + "  " + reader3[3].ToString() + "\n";
								}
								reader3.Dispose();  
								reader3.Close();
								Result.Text+="\n";
							}
						}
					}
					reader2.Dispose();  
					reader2.Close();
					Result.Text+="\n";
				}
				// Beenden des Readers und Freigabe aller Ressourcen.  
				reader.Dispose();  
				reader.Close();
				
				
				connection.Close();									// Datenbank schliessen
				toolStripStatusLabel1.Text="no database open";		// Text für Statuszeile

		}
	}
}
