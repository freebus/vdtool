﻿/*
 * Erstellt mit SharpDevelop.
 * Benutzer: e01a5g22
 * Datum: 02.02.2009
 * Zeit: 08:57
 * 
 * Sie können diese Vorlage unter Extras > Optionen > Codeerstellung > Standardheader ändern.
 */
namespace Test2
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.listBoxErrors = new System.Windows.Forms.ListBox();
			this.openVD = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.Result = new System.Windows.Forms.RichTextBox();
			this.openDB = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 467);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(905, 22);
			this.statusStrip1.TabIndex = 11;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(94, 17);
			this.toolStripStatusLabel1.Text = "no database open";
			// 
			// listBoxErrors
			// 
			this.listBoxErrors.FormattingEnabled = true;
			this.listBoxErrors.HorizontalScrollbar = true;
			this.listBoxErrors.Location = new System.Drawing.Point(12, 382);
			this.listBoxErrors.Name = "listBoxErrors";
			this.listBoxErrors.Size = new System.Drawing.Size(881, 82);
			this.listBoxErrors.TabIndex = 16;
			// 
			// openVD
			// 
			this.openVD.Location = new System.Drawing.Point(12, 12);
			this.openVD.Name = "openVD";
			this.openVD.Size = new System.Drawing.Size(75, 23);
			this.openVD.TabIndex = 17;
			this.openVD.Text = "open VD_";
			this.openVD.UseVisualStyleBackColor = true;
			this.openVD.Click += new System.EventHandler(this.OpenVDClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 23);
			this.label1.TabIndex = 19;
			this.label1.Text = "Table :";
			// 
			// Result
			// 
			this.Result.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Result.Location = new System.Drawing.Point(138, 12);
			this.Result.Name = "Result";
			this.Result.Size = new System.Drawing.Size(755, 364);
			this.Result.TabIndex = 22;
			this.Result.Text = "";
			this.Result.WordWrap = false;
			// 
			// openDB
			// 
			this.openDB.Location = new System.Drawing.Point(12, 119);
			this.openDB.Name = "openDB";
			this.openDB.Size = new System.Drawing.Size(75, 23);
			this.openDB.TabIndex = 23;
			this.openDB.Text = "open DB_";
			this.openDB.UseVisualStyleBackColor = true;
			this.openDB.Click += new System.EventHandler(this.OpenDBClick);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 71);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 24;
			this.label2.Text = "Row :";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(905, 489);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.openDB);
			this.Controls.Add(this.Result);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.openVD);
			this.Controls.Add(this.listBoxErrors);
			this.Controls.Add(this.statusStrip1);
			this.Name = "MainForm";
			this.Text = "vdTool 2.0 - by kubi";
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button openDB;
		private System.Windows.Forms.RichTextBox Result;
		private System.Windows.Forms.ListBox listBoxErrors;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button openVD;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
	}
}
